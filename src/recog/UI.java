package recog;


import java.awt.Color;

import javax.swing.ImageIcon;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

/**
 *
 * @author LENOVO
 */
public class UI extends JFrame implements ActionListener {
    
    JLabel background, background1, icon, acc, id, accountno, name, type, phone, balance, accountnotxt, nametxt, idtxt, typetxt, phonetxt, balancetxt;
    JPanel p1;
    JSeparator id1, accountno1, name1, type1, phone1, balance1;
    JPanel pan1, p2, p3;
    JButton hamburger, enquiry, mini, topup, recharge, pay, crcard;
    boolean status = true;

    public UI() {
        p1 = new JPanel();
        p1.setLayout(null);
        p1.setBounds(0, 0, 800, 800);
        add(p1);

        background = new JLabel(new ImageIcon("G:\\NetBeansProjects\\FaceOn1\\images\\hello.jpg"));
        p1.add(background).setBounds(0, 0, 1370, 760);

        acc = new JLabel("ACCOUNT DETAILS");
        acc.setForeground(Color.WHITE);
        acc.setLocation(800, 20);
        
        acc.setFont(new java.awt.Font("Times New Roman", 0, 32));
        background.add(acc).setBounds(140, 0, 300, 30);

        icon = new JLabel(new ImageIcon("G:\\NetBeansProjects\\FaceOn1\\images\\DatabaseImage\\shreejala.jpg"));
        background.add(icon).setBounds(10, 5, 550, 400);

        id = new JLabel("Account Id");
        id.setForeground(Color.WHITE);
        id.setFont(new java.awt.Font("Times New Roman", 0, 20));
        background.add(id).setBounds(10, 330, 150, 30);

        accountno = new JLabel("Account No.");
        accountno.setForeground(Color.WHITE);
        accountno.setFont(new java.awt.Font("Times New Roman", 0, 20));
        background.add(accountno).setBounds(10, 380, 150, 30);

        name = new JLabel("Account Name");
        name.setForeground(Color.WHITE);
        name.setFont(new java.awt.Font("Times New Roman", 0, 20));
        background.add(name).setBounds(10, 430, 150, 30);

        type = new JLabel("Account Type");
        type.setForeground(Color.WHITE);
        type.setFont(new java.awt.Font("Times New Roman", 0, 20));
        background.add(type).setBounds(10, 480, 150, 30);

        balance = new JLabel("Current Balance");
        balance.setForeground(Color.WHITE);
        balance.setFont(new java.awt.Font("Times New Roman", 0, 20));
        background.add(balance).setBounds(10, 530, 150, 30);

        phone = new JLabel("Contact Number");
        phone.setForeground(Color.WHITE);
        phone.setFont(new java.awt.Font("Times New Roman", 0, 20));
        background.add(phone).setBounds(10, 580, 150, 30);

        idtxt = new JLabel("1030");
        idtxt.setBounds(170, 330, 250, 30);
        idtxt.setForeground(Color.WHITE);
        idtxt.setBorder(null);
        idtxt.setFont(new java.awt.Font("Times New Roman", 0, 20));
        background.add(idtxt);
        idtxt.setOpaque(false);

        accountnotxt = new JLabel("505236952211");
        accountnotxt.setBounds(170, 380, 250, 30);
        accountnotxt.setForeground(Color.WHITE);
        accountnotxt.setBorder(null);
        accountnotxt.setFont(new java.awt.Font("Times New Roman", 0, 20));
        background.add(accountnotxt);
        accountnotxt.setOpaque(false);

        nametxt = new JLabel("Shreejala Tuladhar");
        nametxt.setBounds(170, 430, 250, 30);
        nametxt.setForeground(Color.WHITE);
        nametxt.setBorder(null);
        nametxt.setFont(new java.awt.Font("Times New Roman", 0, 20));
        background.add(nametxt);
        nametxt.setOpaque(false);

        typetxt = new JLabel("Savings");
        typetxt.setBounds(170, 480, 250, 30);
        typetxt.setForeground(Color.WHITE);
        typetxt.setBorder(null);
        typetxt.setFont(new java.awt.Font("Times New Roman", 0, 20));
        background.add(typetxt);
        typetxt.setOpaque(false);

        balancetxt = new JLabel("NRs.6,15,000/-");
        balancetxt.setBounds(170, 530, 250, 30);
        balancetxt.setForeground(Color.WHITE);
        balancetxt.setBorder(null);
        balancetxt.setFont(new java.awt.Font("Times New Roman", 0, 18));
        background.add(balancetxt);
        balancetxt.setOpaque(false);

        phonetxt = new JLabel("9841562378");
        phonetxt.setBounds(170, 580, 250, 30);
        phonetxt.setForeground(Color.WHITE);
        phonetxt.setBorder(null);
        phonetxt.setFont(new java.awt.Font("Times New Roman", 0, 18));
        background.add(phonetxt);
        phonetxt.setOpaque(false);
        
        
        

//        id1=new JSeparator();
//        id1.setBounds(170, 310 , 250, 10);
//        background.add(id1);
//        
//        accountno1=new JSeparator();
//        accountno1.setBounds(170, 360 , 250, 10);
//        background.add(accountno1);
//        
//        name1=new JSeparator();
//        name1.setBounds(170, 410 , 250, 10);
//        background.add(name1);
//        
//        phone1=new JSeparator();
//        phone1.setBounds(170, 460 , 250, 10);
//        background.add(phone1);
//        
//        balance1=new JSeparator();
//        balance1.setBounds(170, 510 , 250, 10);
//        background.add(balance1);
    
        
        
        p2 = new JPanel();
        add(p2).setBounds(730, 0, 40, 40);
        p2.setBackground(Color.WHITE);
        p2.setLayout(null);
        

        ImageIcon ham = new ImageIcon("G:\\NetBeansProjects\\FaceOn1\\images\\hamburger.png");
        hamburger = new JButton(ham);
        p2.add(hamburger).setBounds(0, 0, 40, 40);
        p2.setVisible(true);

        p3 = new JPanel();
        add(p3).setBounds(500, 50, 250, 1000);
        //p3.setOpaque(false);
        p3.setBackground(Color.BLACK);
        p3.setLayout(null);
        p3.hide();

        hamburger.addActionListener(this);

        ImageIcon enqimg = new ImageIcon("G:\\NetBeansProjects\\FaceOn1\\images\\enquiry.png");
        enquiry = new JButton("BALANCE ENQUIRY", enqimg);
        p3.add(enquiry).setBounds(10, 10, 230, 100);

        ImageIcon minimg = new ImageIcon("G:\\NetBeansProjects\\FaceOn1\\images\\mini.png");
        mini = new JButton("MINI STATEMENT", minimg);
        p3.add(mini).setBounds(10, 115, 230, 100);

        ImageIcon topimg = new ImageIcon("G:\\NetBeansProjects\\FaceOn1\\images\\topupp.png");
        topup = new JButton("MOBILE TOPUP", topimg);
        p3.add(topup).setBounds(10, 220, 230, 100);

        ImageIcon recimg = new ImageIcon("G:\\NetBeansProjects\\FaceOn1\\images\\rechargee.png");
        recharge = new JButton("RECHARGE CARDS", recimg);
        p3.add(recharge).setBounds(10, 325, 230, 100);

        ImageIcon payimg = new ImageIcon("G:\\NetBeansProjects\\FaceOn1\\images\\pay.png");
        pay = new JButton("QUICK PAYMENT", payimg);
        p3.add(pay).setBounds(10, 430, 230, 100);

        ImageIcon crimg = new ImageIcon("G:\\NetBeansProjects\\FaceOn1\\images\\crcard.png");
        crcard = new JButton("CREDIT CARD BILL PAYMENT", crimg);
        p3.add(crcard).setBounds(10, 535, 230, 100);

        setSize(800,800);
        setLayout(null);
        setVisible(true);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        //setExtendedState(MAXIMIZED_BOTH);
        setBackground(Color.BLACK);
    }

   

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == hamburger) {
            if (status) {

                p3.show();
                status = false;
            } else {
                p3.hide();
                status = true;
            }

        }
    }

}
